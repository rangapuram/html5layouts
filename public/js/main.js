/**
*  Module
*
* Description
*/
"use strict";
angular.module('angapp', ['ngResource','ngRoute'])
.config(['$routeProvider',function($routeProvider) {

	$routeProvider
	.when('/',{
		templateUrl:'views/components.html',
		controller:'componentsCtrl'
	})
	.when('/layouts', {
		templateUrl:'views/layouts.html',
		controller:'layoutsCtrl'
	})
	.when('/elevators',{
		templateUrl:'views/elevators/elevators.html',
		controller:'elevatorCtrl'
	})
	.otherwise({
		redirectTo:'/'
	});
	
}]).run( function($rootScope, $location){
	
	var path = function(){
		return $location.path();
	}

	$rootScope.$watch(path, function(newValue){
		$rootScope.activeRoute = newValue;

	});

});

requirejs([

	'./js/config.js'	
	],function(){
		angular.bootstrap(document,['angapp']);		
});



