
function move(settings){

	var intervalId = setInterval(function(){
		if(settings.currentFloor < settings.targetFloor){
			settings.currentFloor++;
			this.postMessage(settings.currentFloor);
		}else if(settings.currentFloor > settings.targetFloor){
			settings.currentFloor--;
			this.postMessage(settings.currentFloor);
		}
		else if(settings.currentFloor <= settings.targetFloor){
			this.postMessage("stop");
			clearInterval(intervalId);
		}
	},1000);

}
this.addEventListener('message',function(event){
	move(event.data);
});


