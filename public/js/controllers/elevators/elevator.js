console.log("Entering into JavaScript!");

function ElevatorCommand(){

	var w = null;
	this.run = function(callback){
		console.log("Elevator: " + this.num + " invoked.");
		if(w === null){
			w = new Worker("./js/controllers/elevators/elevatorworker.js");
			console.log("Worker constructed!.");
		}
		var temp = this;
		console.log("Elevator Before Floor: " + temp.currentFloor);
		var temprunning = this.running;
		var tempcurrentFloor = this.currentFloor;
		w.addEventListener('message', function(event){
			if(event.data === "stop"){
				temp.running = false;
				callback(event.data);
				console.log("Elevator After Floor: "+ temp.currentFloor);
			}else{
				temp.currentFloor = event.data;
				callback(event.data);
				console.log("Elevator: "+temp.num + " currentFloor: "+event.data);
			}
		});
		var workerSettings = {};
		workerSettings.num=this.num;
		workerSettings.dir = this.dir;
		workerSettings.currentFloor = this.currentFloor;
		workerSettings.targetFloor = this.targetFloor;
		console.log("Elevator Direction: "+ this.dir +" Elevator currentFloor: "+ this.currentFloor+ " Elevator targetFloor: "+ this.targetFloor);
		workerSettings.num = this.num;
		this.running= true;
		w.postMessage(workerSettings);
	}

}

function Elevator(data){
	this.num = data.num;
	this.currentFloor = data.currentFloor || 1;
	this.targetFloor = data.targetFloor || 1;
	this.running = false;
	ElevatorCommand.call(this);
}

var ELEVATORS ={
	elevatorsList:null,
	getElevators:function(){
		console.log("calling getElevators() --------------------");
		if(this.elevatorsList === null){
			this.elevatorsList = [];
			var el1 = {num:1,currentFloor:0,targetFloor:0};
			var el2 = {num:2,currentFloor:0,targetFloor:0};
			this.elevatorsList.push(new Elevator(el1));
			this.elevatorsList.push(new Elevator(el2));
		}
		return this.elevatorsList;
	},
	getElevatorByNum:function(num){
		console.log("calling getElevatorByNum() --------------------: "+num);
		var el = null;
		this.elevatorsList.every(function(item){
			if(item.num===num){
				el = item;
				return false;
			}else{
				return true;
			}
		});
		console.log("Got Elevator By Num : "+ el.num);
		return el;
	}

};



