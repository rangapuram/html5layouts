
"use strict";
angular.module('angapp')
.controller('elevatorCtrl', ['$scope', function($scope){
	

		$(".floors").on("click", function(event){
		console.log(event.target.tagName);
		if(event.target.tagName === "BUTTON"){
			$(event.target).addClass("highlight");

			var floor = FloorObj.getFloor();
			floor.dir = event.target.name;
			floor.currentFloor = eval(event.target.value);
			var nearbyElevator = null;
			floor.execute(function(value){
				nearbyElevator = value;
				//elevator = nearbyElevator;
				console.log("NearBy elevator : " + nearbyElevator.num);

				if(nearbyElevator !== 'undefined' && nearbyElevator !== null){
					console.log("Calling elevator :"+nearbyElevator.num);
					nearbyElevator.dir = floor.dir;
					nearbyElevator.targetFloor=floor.currentFloor;
					var currentElevatorUI = null;
					$(".elevator .floordisplay").each(function(index, item){
						var elNum = Number($(item).attr("name"));
						if(elNum === nearbyElevator.num){
							currentElevatorUI = item;
						}
					});
					nearbyElevator.run(function(data){
						if(data === "stop"){
							$(event.target).removeClass("highlight");
						}else if(currentElevatorUI !== null && currentElevatorUI !== 'undefined'){
							$(currentElevatorUI).text(data);
						}
					});
				} 
			});
		}

	});


	$(".elevator").on("click",function(event){
		if(event.target.tagName === "BUTTON"){
			var elNum = eval(event.target.name);
			$(event.target).addClass("highlight");
			var elevator = ELEVATORS.getElevatorByNum(elNum);
			if(elevator !== 'undefined' && elevator !== null){
				elevator.targetFloor = event.target.value;
				console.log("Elevator targetFloor : "+ elevator.targetFloor);
				var currentElevatorUI = null;
				$(".elevator .floordisplay").each(function(index, item){
					var elNum = Number($(item).attr("name"));
					if(elNum === elevator.num){
						currentElevatorUI = item;
					}
				});
				elevator.run(function(data){
					if(data === "stop"){
						$(event.target).removeClass("highlight");
					}else if(currentElevatorUI !== null && currentElevatorUI !== 'undefined'){
						$(currentElevatorUI).html(data);
					}

				});
			}
		}
	});
		

function ElevatorCommand(){

	var w = null;
	this.run = function(callback){
		console.log("Elevator: " + this.num + " invoked.");
		if(w === null){
			w = new Worker("./js/controllers/elevators/elevatorworker.js");
			console.log("Worker constructed!.");
		}
		var temp = this;
		console.log("Elevator Before Floor: " + temp.currentFloor);
		var temprunning = this.running;
		var tempcurrentFloor = this.currentFloor;
		w.addEventListener('message', function(event){
			if(event.data === "stop"){
				temp.running = false;
				callback(event.data);
				console.log("Elevator After Floor: "+ temp.currentFloor);
			}else{
				temp.currentFloor = event.data;
				callback(event.data);
				console.log("Elevator: "+temp.num + " currentFloor: "+event.data);
			}
		});
		var workerSettings = {};
		workerSettings.num=this.num;
		workerSettings.dir = this.dir;
		workerSettings.currentFloor = this.currentFloor;
		workerSettings.targetFloor = this.targetFloor;
		console.log("Elevator Direction: "+ this.dir +" Elevator currentFloor: "+ this.currentFloor+ " Elevator targetFloor: "+ this.targetFloor);
		workerSettings.num = this.num;
		this.running= true;
		w.postMessage(workerSettings);
	}

}

function Elevator(data){
	this.num = data.num;
	this.currentFloor = data.currentFloor || 1;
	this.targetFloor = data.targetFloor || 1;
	this.running = false;
	ElevatorCommand.call(this);
}

var ELEVATORS ={
	elevatorsList:null,
	getElevators:function(){
		console.log("calling getElevators() --------------------");
		if(this.elevatorsList === null){
			this.elevatorsList = [];
			var el1 = {num:1,currentFloor:0,targetFloor:0};
			var el2 = {num:2,currentFloor:0,targetFloor:0};
			this.elevatorsList.push(new Elevator(el1));
			this.elevatorsList.push(new Elevator(el2));
		}
		return this.elevatorsList;
	},
	getElevatorByNum:function(num){
		console.log("calling getElevatorByNum() --------------------: "+num);
		var el = null;
		this.elevatorsList.every(function(item){
			if(item.num===num){
				el = item;
				return false;
			}else{
				return true;
			}
		});
		console.log("Got Elevator By Num : "+ el.num);
		return el;
	}

};



var Directions = {up:'up',down:'down'};

function FloorCommand(){

	this.execute = function(callback){
		var elevators = ELEVATORS.getElevators();
		var diff = null;
		var elevatorIndex = null;
		var looping = false;
		var clearIntervalCalled = false;
		var floorFloor = this.currentFloor;
		var setIntervalId = setInterval(function(){
			if(!looping){
				looping = true;
				elevators.forEach(function(el,index){
					if(!el.running){
						console.log("Elevator Floor:  "+ el.currentFloor + " And Floor floor: "+floorFloor);
						if(diff === null){
							diff = Math.abs(el.currentFloor-floorFloor);
							elevatorIndex = index;
						}else if(diff > Math.abs(el.currentFloor-floorFloor)){
							diff = Math.abs(el.currentFloor-floorFloor);
							elevatorIndex = index;
						}
						console.log("Elevator and Floor floorDiff:  "+ diff);
					}
				});
				looping = false;
				if(diff !== null && elevatorIndex !== null){
					clearInterval(setIntervalId);
					clearIntervalCalled = true;	
				}	
			}
			if(clearIntervalCalled){
				var returnElevator = elevators[elevatorIndex];
				console.log("returning elevator : " + returnElevator.num);
				callback(returnElevator);	
			}
			
		},2000);

	}
}

function Floor(data){
	this.currentFloor = data.currentFloor || 1;
	this.dir = data.dir;
	FloorCommand.call(this);
}

var FloorObj={
	floorInstance:null,
	getFloor:function(){
		var data = {};
		if(this.floorInstance === null){
			this.floorInstance= new Floor(data);
		}
		return this.floorInstance;
	}
};




}]);