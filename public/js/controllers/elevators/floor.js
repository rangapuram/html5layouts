

var Directions = {up:'up',down:'down'};

function FloorCommand(){

	this.execute = function(callback){
		var elevators = ELEVATORS.getElevators();
		var diff = null;
		var elevatorIndex = null;
		var looping = false;
		var clearIntervalCalled = false;
		var floorFloor = this.currentFloor;
		var setIntervalId = setInterval(function(){
			if(!looping){
				looping = true;
				elevators.forEach(function(el,index){
					if(!el.running){
						console.log("Elevator Floor:  "+ el.currentFloor + " And Floor floor: "+floorFloor);
						if(diff === null){
							diff = Math.abs(el.currentFloor-floorFloor);
							elevatorIndex = index;
						}else if(diff > Math.abs(el.currentFloor-floorFloor)){
							diff = Math.abs(el.currentFloor-floorFloor);
							elevatorIndex = index;
						}
						console.log("Elevator and Floor floorDiff:  "+ diff);
					}
				});
				looping = false;
				if(diff !== null && elevatorIndex !== null){
					clearInterval(setIntervalId);
					clearIntervalCalled = true;	
				}	
			}
			if(clearIntervalCalled){
				var returnElevator = elevators[elevatorIndex];
				console.log("returning elevator : " + returnElevator.num);
				callback(returnElevator);	
			}
			
		},2000);

	}
}

function Floor(data){
	this.currentFloor = data.currentFloor || 1;
	this.dir = data.dir;
	FloorCommand.call(this);
}

var FloorObj={
	floorInstance:null,
	getFloor:function(){
		var data = {};
		if(this.floorInstance === null){
			this.floorInstance= new Floor(data);
		}
		return this.floorInstance;
	}
};
