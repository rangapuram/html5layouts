$(function(){

	$(".floors").on("click", function(event){
		console.log(event.target.tagName);
		if(event.target.tagName === "BUTTON"){
			$(event.target).addClass("highlight");

			var floor = FloorObj.getFloor();
			floor.dir = event.target.name;
			floor.currentFloor = eval(event.target.value);
			var nearbyElevator = null;
			floor.execute(function(value){
				nearbyElevator = value;
				elevator = nearbyElevator;
				console.log("NearBy elevator : " + nearbyElevator.num);

				if(nearbyElevator !== 'undefined' && nearbyElevator !== null){
					console.log("Calling elevator :"+nearbyElevator.num);
					nearbyElevator.dir = floor.dir;
					nearbyElevator.targetFloor=floor.currentFloor;
					var currentElevatorUI = null;
					$(".elevator .floordisplay").each(function(index, item){
						var elNum = Number($(item).attr("name"));
						if(elNum === nearbyElevator.num){
							currentElevatorUI = item;
						}
					});
					nearbyElevator.run(function(data){
						if(data === "stop"){
							$(event.target).removeClass("highlight");
						}else if(currentElevatorUI !== null && currentElevatorUI !== 'undefined'){
							$(currentElevatorUI).text(data);
						}
					});
				} 
			});
		}

	});


	$(".elevator").on("click",function(event){
		if(event.target.tagName === "BUTTON"){
			var elNum = eval(event.target.name);
			$(event.target).addClass("highlight");
			var elevator = ELEVATORS.getElevatorByNum(elNum);
			if(elevator !== 'undefined' && elevator !== null){
				elevator.targetFloor = event.target.value;
				console.log("Elevator targetFloor : "+ elevator.targetFloor);
				var currentElevatorUI = null;
				$(".elevator .floordisplay").each(function(index, item){
					var elNum = Number($(item).attr("name"));
					if(elNum === elevator.num){
						currentElevatorUI = item;
					}
				});
				elevator.run(function(data){
					if(data === "stop"){
						$(event.target).removeClass("highlight");
					}else if(currentElevatorUI !== null && currentElevatorUI !== 'undefined'){
						$(currentElevatorUI).text(data);
					}

				});
			}
		}
	});

});