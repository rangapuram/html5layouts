"use strict";
/**
*  Module
*
* Description
*/
angular.module('angapp')
.controller('componentsCtrl', ['$scope', function($scope){

	$scope.selectedLayout = null;
	$scope.selectedcomp = null;
	$scope.dashboard = $(".dashboard");
	
	$("input[type='radio']").on("click", function(event){

		if(this.value === 'flow'){
			console.log("flow layout selected!");
			$scope.selectedLayout = 'flow';
		}else if(this.value === 'vertical'){
			console.log("vertical layout selected!");
			$scope.selectedLayout = 'vertical';

		}else if(this.value === 'horizontal'){
			console.log("horizontal layout selected!");
			$scope.selectedLayout = 'horizontal';
		}else if(this.value === 'middle'){
			console.log("middle layout selected!");
			$scope.selectedLayout = 'middle';
		}

		if($scope.selectedcomp){
			$scope.layoutComponent($scope.selectedcomp);
		}

	});


	$scope.layoutComponent = function(target){
		console.log("layout the component!");
		$scope.dashboard.empty();
		$scope.selectedcomp =target;
		var maincont = $("<div/>")
		var cont = $("<div/>");

		if($scope.selectedLayout !== null && $scope.selectedLayout === 'horizontal'){
			maincont.addClass("horizontalcontainer");
			var ht = $(target).height();
			var w = $(target).width();
			maincont.height(ht+35);
			var cw = cont.width();
			cont.height(ht);
			cont.addClass("horizontallayout");
			maincont.append(cont);
			cw = w+10;
			for(var i =0; i < 15; i++){
				cw = cw+w;
				cont.width(cw);
				var comp = $(target).clone()
				comp.addClass("horizontallayoutcomp");
				cont.append(comp);
			}
			$scope.dashboard.append(maincont);

		}else if($scope.selectedLayout !== null && $scope.selectedLayout === 'vertical'){

			maincont.addClass("verticalcontainer");
			var ht = $(target).height();
			var w = $(target).width();
			maincont.width(w+35);
			var ch = cont.height();
			cont.addClass("verticallayout");
			maincont.append(cont);
			ch = ht+10;
			for(var i =0; i < 15; i++){
				ch = ch+ht;
				cont.height(ch);
				var comp = $(target).clone()
				comp.addClass("verticallayoutcomp");
				cont.append(comp);
			}
			$scope.dashboard.append(maincont);			

		}else if($scope.selectedLayout !== null && $scope.selectedLayout === 'flow'){

			maincont.addClass("flowcontainer");
			for(var i =0; i < 15; i++){
				var comp = $(target).clone()
				comp.addClass("flowlayoutcomp");
				maincont.append(comp);
			}
			$scope.dashboard.append(maincont);	


		}else if($scope.selectedLayout !== null && $scope.selectedLayout === 'middle'){
			maincont.addClass("middlecontainer");
			for(var i =0; i < 15; i++){
				var comp = $(target).clone()
				comp.addClass("middlelayoutcomp");
				maincont.append(comp);
			}
			$scope.dashboard.append(maincont);	
		}

	}


}])