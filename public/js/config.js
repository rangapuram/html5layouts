'use strict';
requirejs.config({
    baseUrl: './',
    paths: {
        'underscore'            : 'lib/underscore/underscore-min',
        'angular-bootstrap'     : 'lib/angular-bootstrap/ui-bootstrap-tpls.min',
        'angularSpinner'        : 'lib/angular-spinner/angular-spinner',
        'mastheadCtrl'          : 'js/controllers/masthead',
        'componentsCtrl'        : 'js/controllers/components',
        'elevatorCtrl'          :  'js/controllers/elevators/elevators',
        'basictile'             : 'views/directives/tile',
        'elElevator'            :  'js/controllers/elevators/elevator',
        'elFloor'               :  'js/controllers/elevators/controller',
        'elController'          :  'js/controllers/elevators/floor'
    },
    shim: {

        'underscore'             : { deps: ['jquery'], exports: '_' },
        'angular-bootstrap': ['jquery'],
        'angularSpinner': ['jquery']
        
    }
});

define([
    'jquery',
    'mastheadCtrl',
    'componentsCtrl',
    'elevatorCtrl',
    'basictile',
    'elElevator',
    'elFloor',
    'elController'

], function(){

});