"use strict";
/**
*  Module
*
* Description
*/
angular.module('ng')
.directive('basicTile', [function(){
	// Runs during compile
	


	return {
		// name: '',
		// priority: 1,
		// terminal: true,
		// scope: {}, // {} = isolate, true = child, false/undefined = no change
		controller: function($scope, $element, $attrs, $transclude) {},
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		restrict: 'AE', // E = Element, A = Attribute, C = Class, M = Comment
		// template: '<div>sridhar</div>',
		templateUrl: 'views/directives/components/tile.html',
		// replace: true,
		// transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		link: function($scope, iElm, iAttrs, controller) {

			iElm.on("click", function(event){
				if(event && event.target && event.target.nodeName === "INPUT" && event.target.type==="checkbox"){
					console.log("Target is checkbox");
					$scope.layoutComponent($(event.target).next());
				}


			});
			
		}
	};
}]);